#ifndef CONTOURFINDEROSC_H
#define CONTOURFINDEROSC_H

#include "ofxCv.h"
#include "ofxOscRouterNode.h"

class ContourFinderOsc :
    public ofxCv::ContourFinder,
    public ofxOscRouterNode
{
public:
    ContourFinderOsc();
    virtual ~ContourFinderOsc();
    void processOscCommand(const string& command, const ofxOscMessage& m);
protected:
private:
};

#endif // CONTOURFINDEROSC_H
