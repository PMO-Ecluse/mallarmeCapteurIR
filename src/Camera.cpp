#include "Camera.h"

Camera::Camera() :
    ofxCvPiCam(),
    ofxOscRouterNode("camera")
{
    addOscMethod("saturation");
    addOscMethod("sharpness");
    addOscMethod("contrast");
    addOscMethod("brightness");
    addOscMethod("iso");
    addOscMethod("exposureMeteringMode");
    addOscMethod("exposureCompensation");
    addOscMethod("exposureMode");
    addOscMethod("awbMode");
    addOscMethod("awbGains");
    addOscMethod("imageFX");
    addOscMethod("rotation");
    addOscMethod("flips");
    addOscMethod("shutterSpeed");
}

Camera::~Camera(){}

void Camera::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "saturation"))
    {
        if(validateOscSignature("([if])", m))
        {
            setSaturation((int) ofClamp(getArgAsIntUnchecked(m, 0), -100, 100));
        }
    }
    else if(isMatch(command, "sharpness"))
    {
        if(validateOscSignature("([if])", m))
        {
            setSharpness((int) ofClamp(getArgAsIntUnchecked(m, 0), -100, 100));
        }
    }
    else if(isMatch(command, "contrast"))
    {
        if(validateOscSignature("([if])", m))
        {
            setContrast((int) ofClamp(getArgAsIntUnchecked(m, 0), -100, 100));
        }
    }
    else if(isMatch(command, "brightness"))
    {
        if(validateOscSignature("([if])", m))
        {
            setBrightness((int) ofClamp(getArgAsIntUnchecked(m, 0), 0, 100));
        }
    }
    else if(isMatch(command, "iso"))
    {
        if(validateOscSignature("([if])", m))
        {
            setISO((int) ofClamp(getArgAsIntUnchecked(m, 0), 100, 800));
        }
    }
    else if(isMatch(command, "exposureMeteringMode"))
    {
        if(validateOscSignature("([if])", m))
        {
            setExposureMeteringMode((MMAL_PARAM_EXPOSUREMETERINGMODE_T) ofClamp(getArgAsIntUnchecked(m, 0), 0, 4));
        }
    }
    else if(isMatch(command, "exposureCompensation"))
    {
        if(validateOscSignature("([if])", m))
        {
            setExposureCompensation((int) ofClamp(getArgAsIntUnchecked(m, 0), -10, 10));
        }
    }
    else if(isMatch(command, "exposureMode"))
    {
        if(validateOscSignature("([if])", m))
        {
            setExposureMode((MMAL_PARAM_EXPOSUREMODE_T) ofClamp(getArgAsIntUnchecked(m, 0), 0, 13));
        }
    }
    else if(isMatch(command, "awbMode"))
    {
        if(validateOscSignature("([if])", m))
        {
            setAWBMode((MMAL_PARAM_AWBMODE_T) ofClamp(getArgAsIntUnchecked(m, 0), 0, 10));
        }
    }
    else if(isMatch(command, "awbGains"))
    {
        if(validateOscSignature("([if][if])", m))
        {
            setAWBGains(ofClamp(getArgAsFloatUnchecked(m, 0), 0, 1),
                        ofClamp(getArgAsFloatUnchecked(m, 1), 0, 1)
                       );
        }
    }
    else if(isMatch(command, "imageFX"))
    {
        if(validateOscSignature("([if])", m))
        {
            setImageFX((MMAL_PARAM_IMAGEFX_T) ofClamp(getArgAsIntUnchecked(m, 0), 0, 23));
        }
    }
    else if(isMatch(command, "rotation"))
    {
        if(validateOscSignature("([if])", m))
        {
            setRotation((int) ofClamp(getArgAsIntUnchecked(m, 0), 0, 359));
        }
    }
    else if(isMatch(command, "flips"))
    {
        if(validateOscSignature("([if][if])", m))
        {
            setFlips((int) ofClamp(getArgAsIntUnchecked(m, 0), 0, 1),
                     (int) ofClamp(getArgAsIntUnchecked(m, 1), 0, 1)
                    );
        }
    }
    else if(isMatch(command, "shutterSpeed"))
    {
        if(validateOscSignature("([if])", m))
        {
            setShutterSpeed((int) ofClamp(getArgAsIntUnchecked(m, 0), 0, 330000));
        }
    }
}
