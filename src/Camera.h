#ifndef CAMERA_H
#define CAMERA_H

#include "ofxCvPiCam.h"
#include "ofxOscRouterNode.h"

class Camera : public ofxCvPiCam, public ofxOscRouterNode
{
public:
    Camera();
    virtual ~Camera();
    void processOscCommand(const string& command, const ofxOscMessage& m);
protected:
private:
};

#endif // CAMERA_H
